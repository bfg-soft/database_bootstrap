from setuptools import find_packages, setup

setup(
    name='database_bootstrap',
    version='0.1.0',
    packages=find_packages(),
    url='https://bitbucket.org/bfg-soft/database_bootstrap',
    license='MIT',
    author='BFG-Soft',
    author_email='info@bfg-soft.ru',
    description='Some tools for database bootstraping, based on sqlalchemy '
                'and alembic.',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Database',
        'Topic :: Software Development',
        'Topic :: System :: Systems Administration',
    ],
    install_requires=[
        'sqlalchemy>=1.2.0,<2.0.0',
        'alembic>=1.0.0,<2.0.0',
    ],
    entry_points={
        'console_scripts': [
            'run_database_bootstrap = '
            'database_bootstrap.script:run_database_bootstrap'
        ]
    }
)
