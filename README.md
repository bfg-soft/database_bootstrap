# Database bootstrap tool.

A tool for creating and migrating [sqlalchemy](https://www.sqlalchemy.org/)- and
[alembic](https://alembic.sqlalchemy.org)-drived databases.
 

## Getting started

### Installation

We have no pypi account yet and we have no private pypi repository.
So, you should install this package as usual with `pip` package manager in 
virtualenv or just globally from source:
```bash
pip install git+https://bitbucket.org/bfg-soft/database_bootstrap.git
```
If you want to install specific version or branch, you should execute:
```bash
pip install git+https://bitbucket.org/bfg-soft/database_bootstrap.git@v0.1.0
```


### Content

#### Bootstrap
Manual bootstrap usage example:
```python
from logging import getLogger

from mypackage.model import BaseSaModel
from database_bootstrap import perform_database_bootstrap


def run_bootstrap_manualy():
    perform_database_bootstrap(
        '/path/to/alembic.ini',
        BaseSaModel.metadata,
        logger=getLogger(__name__)
    )
```
Function `run_bootstrap_manualy` can be running anywhere in your application 
`mypackage` after that.


#### Script factory
Most common usage case of database bootstraping is script-based usage before 
application startup, because application usually assumes, that database 
schema already exists.

You can use package `database_bootstrap.ScriptFactory` for it, for example:
```python
from logging import getLogger

from mypackage.model import BaseSaModel
from database_bootstrap import ScriptFactory


def run_script():
    script = ScriptFactory.default_cli_arguments(
        'Some description for script.',
        getLogger(__name__),
        BaseSaModel.metadata,
    )
    script()

if __name__ == '__main__':
    run_script()
```

After that, you can use this script from console, help info available with 
flag `--help`. 

We recommend add it to `entry_points['console_scripts']` of `setup.py` file in 
your application.


## License

This project is licensed under the MIT License - see the 
[LICENSE.txt](LICENSE.txt) file for details.
