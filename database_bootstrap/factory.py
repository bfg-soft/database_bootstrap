from argparse import ArgumentParser, Namespace
from logging import DEBUG, INFO, Logger, basicConfig
from os import getcwd
from os.path import join
from typing import Callable, Optional, Tuple

from sqlalchemy import MetaData
from sqlalchemy.exc import SQLAlchemyError

from .bootstrap import perform_database_bootstrap

__all__ = [
    'ScriptFactory',
    'run_default_database_bootstrap',
]


class ScriptFactory(object):
    """Database bootstrap script factory."""
    def __init__(
        self,
        parser: ArgumentParser,
        logger: Logger,
        metadata: MetaData,
        performer: Callable[[str, MetaData], None] = perform_database_bootstrap
    ) -> None:
        self._parser = parser
        self._logger = logger
        self._metadata = metadata
        self._performer = performer

    def add_argument(self, *args, **kwargs) -> None:
        self._parser.add_argument(*args, **kwargs)

    @classmethod
    def default_cli_arguments(
        cls,
        description: str,
        logger: Logger,
        metadata: MetaData,
        performer: Optional[Callable[[str, MetaData], None]] = None
    ) -> 'ScriptFactory':
        parser = ArgumentParser(
            description=description
        )

        parser.add_argument('-a', '--alembic-config',
                            default=join(getcwd(), 'alembic.ini'),
                            help='Alembic configuration filepath.',
                            required=True)

        if not logger.hasHandlers():
            parser.add_argument('-d', '--debug',
                                help='Debug log level.',
                                default=False, action='store_true')

        if performer is None:
            performer = perform_database_bootstrap

        return cls(parser, logger, metadata, performer=performer)

    def _perform(self,
                 config_filepath: str,
                 metadata: MetaData) -> bool:
        logger = self._logger

        try:
            self._performer(config_filepath, metadata)
            return True

        except SQLAlchemyError as err:
            logger.error('Database error has been occured.')
            logger.exception(err)

        except Exception as err:
            logger.error('Unexpected error has been occured.')
            logger.exception(err)

        return False

    def _process_args(self, args: Namespace) -> Tuple[str, MetaData]:
        """Processing cli args.
        Cli args can be added over ``self.add_argument``,
        that`s why processing of cli args can be overwriten in child classes."""
        return args.alembic_config, self._metadata

    def __call__(self) -> bool:
        args = self._parser.parse_args()

        if hasattr(args, 'debug'):
            basicConfig(
                level=DEBUG if args.debug else INFO,
                format='%(asctime)s %(levelname)s [%(name)s] %(message)s'
            )

        return self._perform(*self._process_args(args))


def run_default_database_bootstrap(*args, **kwargs) -> bool:
    """Default database bootstrap runner."""
    return ScriptFactory.default_cli_arguments(*args, **kwargs)()
