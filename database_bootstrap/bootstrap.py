from logging import Logger, getLogger
from os import R_OK, access
from os.path import isfile
from typing import Optional

from alembic.command import show, stamp, upgrade
from alembic.config import Config
from sqlalchemy import MetaData, create_engine
from sqlalchemy.exc import OperationalError, ProgrammingError

__all__ = [
    'perform_database_bootstrap',
]


_logger = getLogger(__name__)


def perform_database_bootstrap(config_filepath: str,
                               metadata: MetaData,
                               logger: Optional[Logger] = None) -> None:
    """
    Performing database bootstrap.
    If there is no schema in database, we will create it.
    If schema exists, we will update it up to last revision.
    """
    if logger is None:
        logger = _logger

    if not isfile(config_filepath) or not access(config_filepath, R_OK):
        logger.error(
            'Alembic configuration file {!r} does not exist or readable.'
            .format(config_filepath)
        )
        raise FileNotFoundError()

    class LoggingConfig(Config):
        """alembic config class with replased stdout printer."""

        def print_stdout(self, text, *arg):
            if arg:
                text = text % arg

            # Warning: logging will working only with right logging
            # configuration.
            logger.info(text)

    alembic_config = LoggingConfig(config_filepath)

    # We use sync engine for bootstraping, there is not need in async engine.
    sa_engine = create_engine(
        alembic_config.get_main_option('sqlalchemy.url')
    )

    try:
        # noinspection SqlDialectInspection,SqlNoDataSourceInspection
        revision = sa_engine.execute('SELECT * FROM alembic_version').scalar()

    except (ProgrammingError, OperationalError):
        revision = None

    with sa_engine.begin() as connection:
        alembic_config.attributes['connection'] = connection

        if revision is not None:
            upgrade(alembic_config, 'head')
            alembic_config.print_stdout('Database scheme has been updated '
                                        'successfully.')

        else:
            metadata.create_all(checkfirst=True, bind=connection)
            stamp(alembic_config, 'head')
            alembic_config.print_stdout('Database scheme has been created '
                                        'successfully.')

    show(alembic_config, revision)
