import sys
from argparse import Namespace
from importlib import import_module
from logging import getLogger
from operator import attrgetter
from typing import Tuple

from sqlalchemy import MetaData

from .factory import ScriptFactory

__all__ = [
    'run_database_bootstrap',
]


_logger = getLogger(__name__)


class _MetadataScriptFactory(ScriptFactory):
    """Database bootstrap script factory with metadata cli argument."""
    def _process_args(self, args: Namespace) -> Tuple[str, MetaData]:
        module_name, attr_name = args.metadata.split(':')
        module = import_module(module_name)
        metadata = attrgetter(attr_name)(module)
        return args.alembic_config, metadata


def run_database_bootstrap() -> None:
    # noinspection PyTypeChecker
    script = _MetadataScriptFactory.default_cli_arguments(
        'Project independent database bootstrap script.',
        _logger,
        None,
    )
    script.add_argument('-m', '--metadata', required=True,
                        help='Entrypoint-like path to sqlalchemy.MetaData '
                             'project variable.')
    successful = script()
    sys.exit(int(not successful))


if __name__ == '__main__':
    run_database_bootstrap()
